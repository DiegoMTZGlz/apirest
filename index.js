const express = require('express');
const app = express();
const PORT = 4000;

app.use(express.json());

// Ruta para sumar dos números
app.get('/results/:n1/:n2', (req, res) => {
    const { n1, n2 } = req.params;
    const result = parseInt(n1, 10) + parseInt(n2, 10);
    res.send(`Suma = ${result}`);
});

// Ruta para multiplicar dos números (POST)
app.post('/results/:n1/:n2', (req, res) => {
    const { n1, n2 } = req.params;
    const result = parseInt(n1, 10) * parseInt(n2, 10);
    res.send(`Multiplicación = ${result}`);
});

// Ruta para dividir dos números (PUT)
app.put('/results/:n1/:n2', (req, res) => {
    const { n1, n2 } = req.params;
    const result = parseInt(n1, 10) / parseInt(n2, 10);
    res.send(`División = ${result}`);
});

// Ruta para calcular la potencia de un número con números enviados en la ruta (PATCH)
app.patch('/results/:n1/:n2', (req, res) => {
    const { n1, n2 } = req.params;
    const result = Math.pow(parseInt(n1, 10), parseInt(n2, 10));
    res.send(`Potencia = ${result}`);
});

// Ruta para restar dos números
app.delete('/results/:n1/:n2', (req, res) => {
    const { n1, n2 } = req.params;
    const result = parseInt(n1, 10) - parseInt(n2, 10);
    res.send(`Resta = ${result}`);
});

app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto: ${PORT}`);
    console.log(`\nUso: http://localhost:4000/results/n1/n2 \n
    GET /results/:n1/:n2 -> Sumar n1 + n2 \n
    POST /results/:n1/:n2 -> Multiplicar n1 * n2 \n
    PUT /results/:n1/:n2 -> Dividir n1 / n2 \n
    PATCH /results/:n1/:n2 -> Potencia n1 ^ n2 \n
    DELETE /results/:n1/:n2 -> restar n1 - n2`);
});