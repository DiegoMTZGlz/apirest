# Práctica Api Restfull

Práctica donde buscamos crear una api para tablas restfull.

![Solicitudes HTTP](./recursos/restfull.png)


### Prerequisítos

Tener instaladas las siguientes herramientas:

npm
node-js

## Ejecución

Utilizamos node para ejecutar nuestro index.js de la siguiente manera

```
node index.js
```

ó

```
npm start
```

Debería mostrarnos una salida similar a:

```
> apirest@1.0.0 start
> node index.js

Servidor corriendo en el puerto: 4000

Uso: http://localhost:4000/results/n1/n2 

    GET /results/:n1/:n2 -> Sumar n1 + n2 

    POST /results/:n1/:n2 -> Multiplicar n1 * n2 

    PUT /results/:n1/:n2 -> Dividir n1 / n2 

    PATCH /results/:n1/:n2 -> Potencia n1 ^ n2 

    DELETE /results/:n1/:n2 -> restar n1 - n2
```

## Elaborado con:

* [Node-js](https://nodejs.org/es)
* [npm](https://www.npmjs.com/)
* [npm <-express ](https://www.npmjs.com/package/express)


## Autor:

* **Diego Martínez** - [DiegoMTZGlz](https://gitlab.com/DiegoMTZGlz)
